package ru.t1.aayakovlev.tm.config;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.aayakovlev.tm")
public class ServerConfig {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull @Autowired DatabaseProperty databaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseURL());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DLL());
        settings.put(SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        settings.put(DEFAULT_SCHEMA, databaseProperty.getDatabaseSchema());

        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getUseRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getHZConfFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public EntityManager getEntityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
