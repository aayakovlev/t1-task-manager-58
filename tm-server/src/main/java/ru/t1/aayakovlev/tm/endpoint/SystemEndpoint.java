package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerHostnameRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerHostnameResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface SystemEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "SystemEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static SystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static SystemEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, SystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static SystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, SystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    );

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    );

    @NotNull
    @WebMethod
    ServerHostnameResponse getHostname(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerHostnameRequest request
    );

}
