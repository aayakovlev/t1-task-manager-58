package ru.t1.aayakovlev.tm.repository.dto.impl;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class TaskDTORepositoryImpl extends AbstractExtendedDTORepository<TaskDTO> implements TaskDTORepository {

    @NotNull
    @Override
    protected Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";

    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return save(task);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return save(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId and e.projectId = :projectId";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId and e.projectId = :projectId";
        entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
