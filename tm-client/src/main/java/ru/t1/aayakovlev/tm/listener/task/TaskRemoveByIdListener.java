package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    public static final String DESCRIPTION = "Task remove by id.";

    public static final String NAME = "task-remove-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(id);

        taskEndpoint.removeTaskById(request);
    }

}
