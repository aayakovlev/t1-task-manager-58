package ru.t1.aayakovlev.tm.repository.model.impl;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class SessionRepositoryImpl extends AbstractExtendedRepository<Session> implements SessionRepository {

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

    @Override
    protected @NotNull String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";

    }

}
