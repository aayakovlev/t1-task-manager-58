package ru.t1.aayakovlev.tm.repository.dto.impl;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;

import java.util.Comparator;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class SessionDTORepositoryImpl extends AbstractExtendedDTORepository<SessionDTO>
        implements SessionDTORepository {

    @NotNull
    @Override
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";
    }

}
