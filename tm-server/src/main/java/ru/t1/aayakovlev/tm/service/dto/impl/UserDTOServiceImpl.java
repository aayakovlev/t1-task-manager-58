package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.UserEmailExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserLoginExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.exception.field.RoleEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.ProjectDTORepositoryImpl;
import ru.t1.aayakovlev.tm.repository.dto.impl.SessionDTORepositoryImpl;
import ru.t1.aayakovlev.tm.repository.dto.impl.TaskDTORepositoryImpl;
import ru.t1.aayakovlev.tm.repository.dto.impl.UserDTORepositoryImpl;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
public final class UserDTOServiceImpl extends AbstractBaseDTOService<UserDTO, UserDTORepository> implements UserDTOService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Override
    protected UserDTORepository getRepository() {
        return context.getBean(UserDTORepositoryImpl.class);
    }

    @NotNull
    private ProjectDTORepository getProjectRepository() {
        return context.getBean(ProjectDTORepositoryImpl.class);
    }

    @NotNull
    private TaskDTORepository getTaskRepository() {
        return context.getBean(TaskDTORepositoryImpl.class);
    }

    @NotNull
    private SessionDTORepository getSessionRepository() {
        return context.getBean(SessionDTORepositoryImpl.class);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @Nullable UserDTO resultModel;
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (isEmailExists(login)) throw new UserEmailExistsException();
        @Nullable UserDTO resultModel;
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @Nullable UserDTO resultModel;
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), role);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser;
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return resultUser;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO resultUser;
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return resultUser;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            return modelRepository.isLoginExists(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            return modelRepository.isEmailExists(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void remove(@Nullable final UserDTO model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final UserDTORepository modelRepository = getRepository();
        @NotNull final TaskDTORepository taskDTORepository = getTaskRepository();
        @NotNull final ProjectDTORepository projectDTORepository = getProjectRepository();
        @NotNull final SessionDTORepository sessionDTORepository = getSessionRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskDTORepository.clear(model.getId());
            projectDTORepository.clear(model.getId());
            sessionDTORepository.clear(model.getId());
            modelRepository.removeById(model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO resultUser = findById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    public @NotNull UserDTO update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        @NotNull final UserDTO user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
