package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.endpoint.TaskEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.endpoint.TaskEndpoint")
public final class TaskEndpointImpl extends AbstractEndpoint implements TaskEndpoint {

    @NotNull
    @Autowired
    private TaskDTOService taskService;

    @NotNull
    @Autowired
    private ProjectTaskDTOService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = projectTaskService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task = taskService.changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.changeStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        taskService.removeById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowAllResponse showAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowAllRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId, sort);
        return new TaskShowAllResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse showTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByProjectIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.changeStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.update(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

}
