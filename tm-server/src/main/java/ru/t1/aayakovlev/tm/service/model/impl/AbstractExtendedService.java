package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.model.ExtendedRepository;
import ru.t1.aayakovlev.tm.service.model.ExtendedService;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public abstract class AbstractExtendedService<E extends AbstractUserOwnedModel, R extends ExtendedRepository<E>>
        extends AbstractBaseService<E, R> implements ExtendedService<E> {

    @NotNull
    @Override
    public E save(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable E resultEntity;
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.save(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public Collection<E> add(@Nullable final String userId, @Nullable final Collection<E> models)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final E entity : models) {
                @NotNull final E resultEntity = entityRepository.save(userId, entity);
                resultEntities.add(resultEntity);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        int result;
        try {
            result = entityRepository.count(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        try {
            findById(userId, id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @Nullable List<E> resultEntities;
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            resultEntities = entityRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        @Nullable List<E> resultEntities;
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            resultEntities = entityRepository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable E resultEntity;
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            resultEntity = entityRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
        if (resultEntity == null) throw new EntityNotFoundException();
        return resultEntity;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new EntityNotFoundException();
        @NotNull final R entityRepository = getRepository();
        @NotNull final EntityManager entityManager = entityRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public E update(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        return update(model);
    }

}
