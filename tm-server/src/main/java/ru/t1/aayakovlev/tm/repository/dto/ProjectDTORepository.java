package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

public interface ProjectDTORepository extends ExtendedDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

}
