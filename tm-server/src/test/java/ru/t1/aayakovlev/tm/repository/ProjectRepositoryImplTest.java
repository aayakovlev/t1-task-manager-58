//package ru.t1.aayakovlev.tm.repository;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import lombok.SneakyThrows;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.MeasureHelper;
//import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
//import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
//import ru.t1.aayakovlev.tm.repository.dto.impl.ProjectDTORepositoryImpl;
//import ru.t1.aayakovlev.tm.service.ConnectionService;
//import ru.t1.aayakovlev.tm.service.PropertyService;
//import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
//import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
//import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
//import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;
//
//@Category(UnitCategory.class)
//public final class ProjectRepositoryImplTest extends AbstractSchemeTest {
//
//    @NotNull
//    private static PropertyService propertyService;
//
//    @NotNull
//    private static ConnectionService connectionService;
//
//    @NotNull
//    private static EntityManager entityManager;
//
//    @NotNull
//    private static ProjectDTORepository repository;
//
//    @NotNull
//    private static ProjectDTOService service;
//
//    @Nullable
//    private static UserDTOService userService;
//
//    @BeforeClass
//    public static void initConnectionService() throws LiquibaseException {
//        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        propertyService = new PropertyServiceImpl();
//        connectionService = new ConnectionServiceImpl(propertyService);
//        entityManager = connectionService.getEntityManager();
//        repository = new ProjectDTORepositoryImpl(entityManager);
//        service = new ProjectDTOServiceImpl(connectionService);
//        userService = new UserDTOServiceImpl(connectionService, propertyService);
//    }
//
//    @AfterClass
//    public static void destroyConnection() {
//        connectionService.close();
//    }
//
//    @Before
//    public void initData() throws AbstractException {
//        userService.save(COMMON_USER_ONE);
//        userService.save(ADMIN_USER_ONE);
//        service.save(PROJECT_USER_ONE);
//        service.save(PROJECT_USER_TWO);
//    }
//
//    @After
//    public void after() throws AbstractException {
//        service.clear();
//        userService.clear();
//    }
//
//    @Test
//    public void When_FindByIdExistsProject_Expect_ReturnProject() {
//        @Nullable final ProjectDTO project = repository.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
//        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
//        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
//        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
//        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
//    }
//
//    @Test
//    public void When_FindByIdExistsProject_Expect_ReturnNull() {
//        @Nullable final ProjectDTO project = repository.findById(USER_ID_NOT_EXISTED, PROJECT_ID_NOT_EXISTED);
//        Assert.assertNull(project);
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_SaveNotNullProject_Expect_ReturnProject() {
//        MeasureHelper.measure(entityManager, () -> repository.save(PROJECT_ADMIN_ONE));
//        @Nullable final ProjectDTO project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getDescription(), project.getDescription());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getName(), project.getName());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getStatus(), project.getStatus());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getUserId(), project.getUserId());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getCreated(), project.getCreated());
//    }
//
//    @Test
//    public void When_CountCommonUserProjects_Expect_ReturnTwo() {
//        final int count = repository.count(COMMON_USER_ONE.getId());
//        Assert.assertEquals(2, count);
//    }
//
//    @Test
//    public void When_FindAllUserId_Expected_ReturnListProjects() {
//        final List<ProjectDTO> projects = repository.findAll(COMMON_USER_ONE.getId());
//        for (int i = 0; i < projects.size(); i++) {
//            Assert.assertEquals(USER_PROJECT_LIST.get(i).getName(), projects.get(i).getName());
//        }
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveExistedProject_Expect_FindNull() {
//        MeasureHelper.measure(entityManager, () -> repository.save(PROJECT_ADMIN_ONE));
//        @Nullable final ProjectDTO project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
//        Assert.assertNull(project);
//
//        MeasureHelper.measure(entityManager, () -> repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId()));
//        @Nullable final ProjectDTO projectAfterDelete = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
//        Assert.assertNull(projectAfterDelete);
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveAll_Expect_ZeroCountProjects() {
//        MeasureHelper.measure(entityManager, () -> repository.save(PROJECT_ADMIN_ONE));
//
//        MeasureHelper.measure(entityManager, () -> repository.save(PROJECT_ADMIN_TWO));
//
//        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
//
//        MeasureHelper.measure(entityManager, () -> repository.clear(ADMIN_USER_ONE.getId()));
//
//        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
//    }
//
//    @Test
//    @SneakyThrows
//    public void When_RemoveByIdExistedProject_Expect_NullObject() {
//        MeasureHelper.measure(entityManager, () -> repository.save(PROJECT_ADMIN_TWO));
//        MeasureHelper.measure(entityManager, () -> repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId()));
//
//        @Nullable final ProjectDTO project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
//        Assert.assertNull(project);
//    }
//
//    @Test
//    public void When_RemoveNotExistsProject_Expect_Nothing() {
//        MeasureHelper.measure(entityManager, () -> {
//            repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId());
//        });
//    }
//
//}
