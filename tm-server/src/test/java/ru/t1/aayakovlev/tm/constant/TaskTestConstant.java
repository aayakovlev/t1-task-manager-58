package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

public final class TaskTestConstant {

    @NotNull
    public final static TaskDTO TASK_USER_ONE = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_USER_TWO = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_ADMIN_ONE = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_ADMIN_TWO = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_EMPTY_PROJECT_ID = new TaskDTO();

    @NotNull
    public final static TaskDTO TASK_NOT_EXISTED = new TaskDTO();

    @NotNull
    public final static String TASK_ID_NOT_EXISTED = TASK_NOT_EXISTED.getId();

    @NotNull
    public final static String NAME = "Expected Task name";

    @NotNull
    public final static String DESCRIPTION = "Expected Task Description";

    @NotNull
    public final static List<TaskDTO> ADMIN_TASK_LIST = Arrays.asList(TASK_ADMIN_ONE, TASK_ADMIN_TWO, TASK_EMPTY_PROJECT_ID);

    @NotNull
    public final static List<TaskDTO> USER_TASK_LIST = Arrays.asList(TASK_USER_ONE, TASK_USER_TWO);

    @NotNull
    public final static List<TaskDTO> USER_TASK_SORTED_LIST = new ArrayList<>(USER_TASK_LIST);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    @NotNull
    public final static List<TaskDTO> TASK_SORTED_LIST = new ArrayList<>();

    static {
        TASK_ADMIN_ONE.setProjectId(PROJECT_ADMIN_ONE.getId());
        TASK_ADMIN_TWO.setProjectId(PROJECT_ADMIN_TWO.getId());

        TASK_USER_ONE.setProjectId(PROJECT_USER_ONE.getId());
        TASK_USER_TWO.setProjectId(PROJECT_USER_TWO.getId());

        ADMIN_TASK_LIST.forEach(
                task -> {
                    task.setName("Test Admin Task " + task.hashCode());
                    task.setDescription("Test Admin Task " + task.hashCode());
                    task.setUserId(ADMIN_USER_ONE.getId());
                }
        );
        USER_TASK_LIST.forEach(
                task -> {
                    task.setName("Test User Task " + task.hashCode());
                    task.setDescription("Test User Task " + task.hashCode());
                    task.setUserId(COMMON_USER_ONE.getId());
                }
        );

        TASK_LIST.addAll(ADMIN_TASK_LIST);
        TASK_LIST.addAll(USER_TASK_LIST);

        USER_TASK_SORTED_LIST.sort(NameComparator.INSTANCE);

        TASK_SORTED_LIST.addAll(TASK_LIST);
        TASK_SORTED_LIST.sort(NameComparator.INSTANCE);
    }

}
