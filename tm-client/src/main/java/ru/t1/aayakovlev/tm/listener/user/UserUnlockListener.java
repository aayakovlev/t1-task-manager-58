package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserUnlockRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Unlock user by login.";

    @NotNull
    public static final String NAME = "user-unlock";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userUnlockListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();

        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);

        userEndpoint.unlock(request);
    }

}
