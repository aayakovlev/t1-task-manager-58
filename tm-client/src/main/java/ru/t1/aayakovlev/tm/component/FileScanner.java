package ru.t1.aayakovlev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.listener.AbstractListener;
import ru.t1.aayakovlev.tm.service.LoggerService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private ClientBootstrap bootstrap;

    @NotNull
    @Autowired
    private LoggerService loggerService;

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public void init() {
        listeners.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        @Nullable final File[] files = folder.listFiles();
        if (files == null) return;
        for (@NotNull final File file : files) {
            if (file.isDirectory()) continue;
            @NotNull final String filename = file.getName();
            final boolean check = commands.contains(filename);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(filename);
                } catch (@NotNull final Exception e) {
                    loggerService.error(e);
                }
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

}
