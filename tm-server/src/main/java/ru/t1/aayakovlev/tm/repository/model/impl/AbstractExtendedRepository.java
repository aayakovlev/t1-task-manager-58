package ru.t1.aayakovlev.tm.repository.model.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ExtendedRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractExtendedRepository<E extends AbstractUserOwnedModel>
        extends AbstractBaseRepository<E> implements ExtendedRepository<E> {

    @NotNull
    @Override
    public E save(
            @NotNull final String userId,
            @NotNull final E model
    ) {
        model.setUser(entityManager.find(User.class, userId));
        return save(model);
    }

    @Override
    public int count(@NotNull final String userId) {
        @NotNull final String query = "select count(*) from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        return entityManager.createQuery(query, Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId order by " + getSortColumnName(comparator);
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.userId = :userId and e.id = :id";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.id = :id and e.userId = :userId";
        entityManager.createQuery(query)
                .executeUpdate();
    }

    @NotNull
    @Override
    public E update(@NotNull final String userId, @NotNull final E model) {
        model.setUser(entityManager.find(User.class, userId));
        return entityManager.merge(model);

    }

}
