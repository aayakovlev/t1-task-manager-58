package ru.t1.aayakovlev.tm.repository.model;

import ru.t1.aayakovlev.tm.model.Session;

public interface SessionRepository extends ExtendedRepository<Session> {

}
